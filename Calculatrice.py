from tkinter import *

root = Tk()
root.title("Calculatrice simple")

e = Entry(root, width = 40, borderwidth = 0, justify="center")
e.grid(row=0, column = 0, columnspan=4, padx = 10, pady = 10)

def buttonPress(nbr):
    encours = e.get()
    e.delete(0,END)
    e.insert(0, str(encours) + str(nbr))

def buttonClear ():
    e.delete(0, END)

def buttonAddi():
    nbr1 = e.get()
    global nbrUno
    global math
    math = "addition"
    nbrUno = int(nbr1)
    e.delete(0, END)
    
def buttonSoustra():
    nbr1 = e.get()
    global nbrUno
    global math
    math = "soustraction"
    nbrUno = int(nbr1)
    e.delete(0, END)

def buttonMulti():
    nbr1 = e.get()
    global nbrUno
    global math
    math = "multiplication"
    nbrUno = int(nbr1)
    e.delete(0, END)

def buttonDiv():
    nbr1 = e.get()
    global nbrUno
    global math
    math = "division"
    nbrUno = int(nbr1)
    e.delete(0, END)

def buttonEgal():
    nbrDos = e.get()
    e.delete(0, END)

    if math == "addition":
        e.insert(0, nbrUno + int(nbrDos))
    
    if math == "soustraction":
        e.insert(0, nbrUno - int(nbrDos))

    if math == "division":
        e.insert(0, nbrUno / int(nbrDos))

    if math == "multiplication":
        e.insert(0, nbrUno * int(nbrDos))

#definition des boutons

button1 = Button(root, text="1", command = lambda: buttonPress(1), padx= 35, pady= 15)#lambda : equivalent de ONCLICK
button2 = Button(root, text="2", command = lambda: buttonPress(2), padx= 35, pady= 15)
button3 = Button(root, text="3", command = lambda: buttonPress(3), padx= 35, pady= 15)
button4 = Button(root, text="4", command = lambda: buttonPress(4), padx= 35, pady= 15)
button5 = Button(root, text="5", command = lambda: buttonPress(5), padx= 35, pady= 15)
button6 = Button(root, text="6", command = lambda: buttonPress(6), padx= 35, pady= 15)
button7 = Button(root, text="7", command = lambda: buttonPress(7), padx= 35, pady= 15)
button8 = Button(root, text="8", command = lambda: buttonPress(8), padx= 35, pady= 15)
button9 = Button(root, text="9", command = lambda: buttonPress(9), padx= 35, pady= 15)
button0 = Button(root, text="0", command = lambda: buttonPress(0), padx= 35, pady= 15)

buttonPlus = Button(root, text="+", command = buttonAddi, padx= 33.3, pady= 15)
buttonMulti = Button(root, text="X", command = buttonMulti, padx= 33.3, pady= 15)
buttonSoustra = Button(root, text="-", command = buttonSoustra, padx= 33.3, pady= 15)
buttonDiv = Button(root, text="/", command = buttonDiv, padx= 33.3, pady= 15)

buttonClear = Button(root, text="C", command = buttonClear , padx= 78, pady= 15)# rappel: ne pas appeler la fct avec ()
buttonEgal = Button(root, text="=", command = buttonEgal , padx= 150, pady= 15)

#mise des boutons sur lecran
button1.grid(row = 3, column = 0 )
button2.grid(row = 3, column = 1 )
button3.grid(row = 3, column = 2 )

button4.grid(row = 2, column = 0 )
button5.grid(row = 2, column = 1 )
button6.grid(row = 2, column = 2 )

button7.grid(row = 1, column = 0 )
button8.grid(row = 1, column = 1 )
button9.grid(row = 1, column = 2 )

button0.grid(row = 4, column = 0 )

buttonClear.grid(row = 4, column = 1 , columnspan=2)
buttonEgal.grid(row = 5, column = 0 , columnspan=4, sticky= W+E)

buttonPlus.grid(row = 1, column = 3 )
buttonSoustra.grid(row = 4, column = 3 )
buttonMulti.grid(row = 2, column = 3 )
buttonDiv.grid(row = 3, column = 3 )

root.mainloop()
